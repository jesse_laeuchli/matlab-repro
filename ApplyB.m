function [y] = ApplyB(data,outputP,B,pbound)
probs =[];
n=min(size(data))+1 ;
for(i=1:max(size(data)))
    
    if(isnan(data(i,3))==1)
        data(i,3)=0;
    end
    v= B(1)+dot(B(2:n),data(i,:));
    p= Logistic(v );

    probs = [probs;p];
    
end

newprobs=[];
if(outputP>0)
for(i=1:max(size(probs)))
    if(probs(i) > pbound)
    
        newprobs = [newprobs 1];
    else
        newprobs = [newprobs 0];
    end
    
end
end

probs = newprobs;

y = probs;