TrainingData=xlsreadstruct('train.csv');

Y=TrainingData.Survived;
ylist=[Y ones(891,1)];
c1=TrainingData.Pclass;
c2s=TrainingData.Sex;
c3=TrainingData.Age;

c4=TrainingData.SibSp;
c5=TrainingData.Parch;


c2 =[];

for(i=1:892)
c2(i)=strcmp('male',c2s(i))
end
c2=c2';

Data=[c1 c2 c3 c4 c5];
B = glmfit(Data, ylist, 'binomial', 'link', 'logit');

TestData=xlsreadstruct('test.csv')
c1t=TestData.Pclass;
c2st=TestData.Sex;
c2t =[];
for(i=1:418)
c2t(i)=strcmp('male',c2st(i));
end

c3t=TestData.Age;

c4t=TestData.SibSp;
c5t=TestData.Parch;
DataTest=[c1t c2t' c3t c4t c5t];

outputlife=ApplyB(DataTest,1,B,.71);

csvwrite('testrun5paramsprob71.csv',outputlife');